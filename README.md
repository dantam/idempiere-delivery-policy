# README #

This plugin is used to add a first order, first delivery scheme to iDempiere. Normally iDempiere delivers whatever is in stock to whoever
gets their shipment completed first. 

The goal of this plugin is to have the one who put the order first gets the article first, regardless of when the shipment is
actually made.

### How do I get set up? ###

See https://bitbucket.org/dantam/idempiere-delivery-policy/wiki/Setup

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
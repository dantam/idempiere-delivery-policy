package se.idempiere.deliverypolicy.validator;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MClient;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MSysConfig;
import org.compiere.model.ModelValidationEngine;
import org.compiere.model.ModelValidator;
import org.compiere.model.PO;
import org.compiere.util.CLogger;

import se.idempiere.deliverypolicy.Activator;
import se.idempiere.deliverypolicy.process.SalesOrderAllocation;


public class MMaterialReceiptValidator implements ModelValidator {

	private int		m_AD_Client_ID = -1;
	
	private CLogger log = CLogger.getCLogger(this.getClass());	
	
	
	@Override
	public void initialize(ModelValidationEngine engine, MClient client) {
		
		if (client!=null) m_AD_Client_ID = client.getAD_Client_ID();
		
		// Run allocation on prepare
		engine.addDocValidate(MInOut.Table_Name, this);
		log.info(this.getClass() + " initialized.");
		System.out.println("Init client id = " + m_AD_Client_ID);
		
	}

	@Override
	public int getAD_Client_ID() {
		return m_AD_Client_ID;
	}

	@Override
	public String login(int AD_Org_ID, int AD_Role_ID, int AD_User_ID) {
		return null;
	}

	@Override
	public String modelChange(PO po, int type) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String docValidate(PO po, int timing) {
		
		if (timing != ModelValidator.TIMING_AFTER_COMPLETE)
			return null;
		
		// Make sure we have settings that needs allocation
		boolean salesOrderAllocationEnabled = MSysConfig.getBooleanValue(Activator.SALES_ORDERLINE_ALLOCATION, false, po.getAD_Client_ID());
		if (!salesOrderAllocationEnabled)
			return null;

		MInOut o = (MInOut)po;
		
		// Not drop shipment
		if (o.isDropShip())
			return null;
		
		// Only incoming
		if (MDocType.DOCBASETYPE_MaterialDelivery.equals(o.getC_DocType().getDocBaseType())) {
			return null;
		}
		
		if (timing == ModelValidator.TIMING_AFTER_COMPLETE) {
			try {
				SalesOrderAllocation.runSalesOrderAllocation(null, log, po.getCtx(), o.getM_Warehouse_ID(), 0, 0, o.get_TrxName());
			} catch (Exception e) {
				throw new AdempiereException(e);
			}
		}
		
		return null;
	}

}

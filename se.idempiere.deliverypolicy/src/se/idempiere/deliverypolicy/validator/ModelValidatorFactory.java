package se.idempiere.deliverypolicy.validator;

import org.adempiere.base.IModelValidatorFactory;
import org.compiere.model.ModelValidator;

public class ModelValidatorFactory implements IModelValidatorFactory {

	@Override
	public ModelValidator newModelValidatorInstance(String className) {

		if ("se.idempiere.deliverypolicy.validator.MMaterialReceiptValidator".equals(className)) {
			return new MMaterialReceiptValidator();
		}
		
		if ("se.idempiere.premium.validator.MMaterialReceiptValidator".equals(className)) {
			return new MMaterialReceiptValidator();
		}
		
		if ("se.idempiere.deliverypolicy.validator.MOrderValidator".equals(className)) {
			return new MOrderValidator();
		}
		
		if ("se.idempiere.premium.validator.MOrderValidator".equals(className)) {
			return new MOrderValidator();
		}
		
		
		return null;
	}

}

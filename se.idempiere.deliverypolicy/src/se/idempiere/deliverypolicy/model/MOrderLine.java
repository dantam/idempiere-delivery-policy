package se.idempiere.deliverypolicy.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.util.Env;
import org.compiere.util.Msg;

public class MOrderLine extends org.compiere.model.MOrderLine {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6246927090095394810L;
	
	public MOrderLine(Properties ctx, int id, String trxName) {
		super(ctx, id, trxName);
	}
	
	
	public MOrderLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	/** Set Qty Allocated.
		@param QtyAllocated 
		Qty allocated from inventory on hand
	  */
	public void setQtyAllocated (BigDecimal QtyAllocated)
	{
		set_ValueNoCheck (X_C_OrderLine.COLUMNNAME_QtyAllocated, QtyAllocated);
	}
	
	/** Get Qty Allocated.
		@return Qty allocated from inventory on hand
	  */
	public BigDecimal getQtyAllocated () 
	{
		BigDecimal bd = (BigDecimal)get_Value(X_C_OrderLine.COLUMNNAME_QtyAllocated);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	
	/**
	 * Tries to allocate as much as possible (delivered - ordered).
	 * Allocated only as much as is reserved.
	 * 
	 * @param trxName
	 * @return
	 */
	public BigDecimal allocateOnHand(String trxName) {
		BigDecimal toDeliver = this.getQtyOrdered().subtract(getQtyDelivered());
		if (toDeliver.signum()==1) {
			return(allocateOnHand(toDeliver.min(getQtyReserved()), trxName)); 
		} else {
			return Env.ZERO;
		}
	}
	
	/**
	 * Allocates at most 'toAllocate' number of items.
	 * If the toAllocate is a negative number material is unallocated.
	 * 
	 * @return	Number allocated
	 */
	public BigDecimal allocateOnHand(BigDecimal toAllocate, String trxName) {

		if (BigDecimal.ZERO.equals(toAllocate))
			return BigDecimal.ZERO;

		boolean reverse = toAllocate.signum()<0;

		if (!reverse) {
			setQtyAllocated(getQtyAllocated().add(toAllocate));
		} else {
			setQtyAllocated(getQtyAllocated().subtract(toAllocate));
		}
		
		if (getQtyAllocated().signum()<0) {
			setQtyAllocated(Env.ZERO);
		}
		if (getQtyAllocated().doubleValue()>(getQtyOrdered().subtract(getQtyDelivered()).doubleValue())) {
			setQtyAllocated(getQtyOrdered().subtract(getQtyDelivered()));
		}
		
		saveEx(trxName);
		
		return(toAllocate);
	}


	/* (non-Javadoc)
	 * @see org.compiere.model.MOrderLine#beforeDelete()
	 */
	@Override
	protected boolean beforeDelete() {
		if (Env.ZERO.compareTo(getQtyReserved()) != 0)
		{
			// Unreserve
			setQty(Env.ZERO);
			((se.idempiere.deliverypolicy.model.MOrder)getC_Order()).reserveStock(null, new MOrderLine[]{this});
			//	For PO should be On Order
			if (!getQtyReserved().equals(Env.ZERO )) {
				log.saveError("DeleteError", Msg.translate(getCtx(), "QtyReserved") + "=" + getQtyReserved());
				return false;
			}
		}
		
		if (Env.ZERO.compareTo(getQtyAllocated()) != 0) {
			// Unallocate
			allocateOnHand(getQtyAllocated().negate(), get_TrxName());
		}

		
		return super.beforeDelete();
	}


	/* (non-Javadoc)
	 * @see org.compiere.model.MOrderLine#beforeSave(boolean)
	 */
	@Override
	protected boolean beforeSave(boolean newRecord) {
		/**
		 * Unreserve stock if product is changed
		 */
		if (Env.ZERO.compareTo(getQtyReserved()) != 0 && is_ValueChanged(COLUMNNAME_M_Product_ID))
		{
			// Unreserve
			setQty(Env.ZERO);
			((se.idempiere.deliverypolicy.model.MOrder)getC_Order()).reserveStock(null, new MOrderLine[]{this});
			//	For PO should be On Order
			if (!getQtyReserved().equals(Env.ZERO )) {
				log.saveError("DeleteError", Msg.translate(getCtx(), "QtyReserved") + "=" + getQtyReserved());
				return false;
			}
		}
		
		if (Env.ZERO.compareTo(getQtyAllocated()) != 0 && is_ValueChanged(COLUMNNAME_M_Product_ID)) {
			// Unallocate
			allocateOnHand(getQtyAllocated().negate(), get_TrxName());
		}
		return super.beforeSave(newRecord);
	}
	
	

}
